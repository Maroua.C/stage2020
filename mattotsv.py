"""
Created on Tue Apr 21 16:44:01 2020
@author: Maroua, Ozan
"""

import cobra.test
import cobra
import cobra.core.Reaction
import pandas as pd

##using panda librairie for reading the file which come from HGNC and relie Gene ID and gene names 
df=pd.read_csv('HGNCGeneEntrez.txt', sep='\t', dtype={'ApprovedSymbol':str, 'NCBI':str})
##remove the first col of df 
df=df.set_index('NCBI')
##df convert to a dictionnary
entrezToGene=df.to_dict()
##change the key by ApprovedSymbol size 1 to 41k
entrezToGene=entrezToGene['ApprovedSymbol']

##LOAD the mat file thank to cobrapy
model=cobra.io.load_matlab_model("Recon3D_301.mat")

#unipartite is reactant -> product
unipartite=set()

#bipartite is reactant -> reaction -> product mais pas de gene
bipartite=set()

#tripartite is reactant and gene -> reaction -> produc
tripartite=set()

#contains reactants -> products  and genes-> products so reactions are not there
unipartitePlusGenes=set()

#to better discriminate between genes metabolites and reactions
nodeTypes=set()


for r in model.reactions:
    
    #without this line , the node types for products were missing.
    #add '_reaction' postfix to the reactions
	rId=r.id+'_reaction'
	nodeTypes.add((rId, 'Reaction'))


	for rt in r.reactants:
        #split to get the id 
		rtId=rt.id.split('[')[0]
		nodeTypes.add((rtId, 'Metabolite'))
        #dans les reactants obtenir les produits (reaction)
		for pr in r.products:
			prodId=pr.id.split('[')[0]
            #reactant(metabolite) -> product(unipartite uniquement)
			unipartite.add((rtId,prodId))
			unipartitePlusGenes.add((rtId,prodId))
#reactions -> reactants(metabolite)
		bipartite.add((rtId,rId))
		tripartite.add((rtId,rId))


	for pr in r.products:
		prodId=pr.id.split('[')[0]
		nodeTypes.add((prodId, 'Metabolite'))
#reactions -> product
		bipartite.add((rId,prodId))
		tripartite.add((rId,prodId))

	for g in r.genes:
		gID=g.id.split('.')[0]
		if gID in entrezToGene:
			gName=entrezToGene[gID]
			nodeTypes.add((gName,'Gene'))
#reactions -> gene
			tripartite.add((gName,rId))
			for pr in r.products:
				prodId=pr.id.split('[')[0]
                #gene et produit
				unipartitePlusGenes.add((gName,prodId))


f=open('nodeTypes.tsv','w')
for t in nodeTypes:
	f.write(t[0]+'\t'+t[1]+'\n')
f.close()

f=open('unipartiteID.tsv','w')
for t in unipartite:
	f.write(t[0]+'\t'+t[1]+'\n')
f.close()

f=open('bipartiteID.tsv','w')
for t in bipartite:
	f.write(t[0]+'\t'+t[1]+'\n')
f.close()

f=open('tripartiteID.tsv','w')
for t in tripartite:
	f.write(t[0]+'\t'+t[1]+'\n')
f.close()

f=open('unipartitePlusGenesID.tsv','w')
for t in unipartitePlusGenes:
	f.write(t[0]+'\t'+t[1]+'\n')
f.close()






"""
## c is for how many metabolites do we have 
c=0
for m in model.metabolites:
	if len(m.reactions)==0:
		c=c+1




"""
