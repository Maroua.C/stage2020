# Internship M1  
# Utilizing metabolic networks to understand rare disease mechanisms


## Installation of librairies 

1.  cobrapy
2.  panda
3.  networkx

Using the  [pip](https://pip.pypa.io/en/stable/) package for  downaling the librairies.
```bash
pip install cobrapy
pip install panda
pip install networkx
```

## Explanation of files
We have two files:  

*  mattotsv.py **Construction of metabolic network using cobrapy**
    * Using COBRApy library and convert the MAT file to an edge list file in TSV format, for the purpose of using this network and performing our analyses.
    
    * Input files needed here: 
       * Recon3D_301.mat
       * HGNCGeneEntrez.txt   
    
     * We were able to reconstruct the following metabolic networks: 
        * a unipartite representation, containing  metabolites as nodes. So we have here directed edges from reactants to products.
        * a bipartite network, which has two types of nodes metabolites and reactions. Also, directed edges connecting reactants to reactions and reactions to products.
        * a tripartite network, containing three types of nodes (metabolites, reactions and genes). Genes refer to “enzymes” that catalyze the reactions. Here edges are connecting reactants and enzymes to reactions and reactions to products.
         
         

-------  

*  analysis.py **Rare disease-oriented network analysis**
    * Making an analysis of the tripartite metabolic network we build on mattotsv.py. Then, search for the specific intersection between vitamin targets and congenital renal rare diseases.
    * Thus, here we are using the library NetworkX and the tripartite and build a workflow. In order to  see  if CAKUT genes are in the metabolic network and also if metabolites play a role in CAKUT.
    * CAKUT are three output files which contain congenital renal genes.

    
    * Input files needed here: 
       * tripartiteID.tsv
       * CAKUT-CausalGenesFromPMC5748921.txt
       * CAKUT-GO0060993KidneyMorphogenesis.txt
       * CAKUT-GO0072001RenalSystemDevelopment.txt
       * vitamin_A.txt
       * vitamin_D.txt

    


## Nb
In the project unipartiteID.tsv, bipartiteID.tsv, tripartiteID.tsv are the output file for mattotsv.py.



    
    
    
    
    