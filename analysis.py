#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  7 15:12:50 2020


predecessors = incomming  to the reaction
sucessor = outgoing nodes 

"""

import networkx as nx

#####################################
#____________Function_______________#
#####################################

def isingraph(G,list):
    """Test if nodes are in the graph and  return a list """
    yeslist=[]
    # bool to know if a metabolic is in the graph as node 
    for element in list:
        if G.has_node(element) == True:
            #same but we use the first one in the loop
            #print(G.has_node('pd3'))
            #print('pd3' in G)

            yeslist.append(element)
        
    return yeslist

        
def filetolist(f):
    """Put data file into a list  """
    tmp=f.readlines()
    #liste of interest 
    list=[]
    for element in tmp:
        list.append(element.rstrip())
        
    f.close()   
    return list


def most_connected(dico):
    """THE MOST CONNECTED NODE """
    max_node="" 
    max_neighbors=0
   
    for key,value in dico.items():
        #print(len(element))
        if max_neighbors < len(value):
            max_neighbors=len(value)
            max_node=key
    print("Max_neighbors and the name of the node : ",max_node,max_neighbors) 
    list=[max_node,max_neighbors]
    
    return list


def first_neighbors(G,onelist):
    """First neighbog we select the outgoing nodes thus the sucessors  """
    outgoing={}
    
    for metabolite in onelist:
        outgoing[metabolite]=list(G.neighbors(metabolite))
        
    return outgoing


def all_reaction(one_list,FN_dico):
    """ looking thought all vit and reactions for the reaction 
        which include the first neighbors of the dico 
    """
    reaction=[]
    for vit in one_list:
        for reacs in FN_dico[vit]:
                reaction.append(reacs)
    return reaction

def second_neighbors(G,reaction):
    """second neighbog we select the incomming nodes thus the predecessors """
    #looking for the neighbors of reactions          
    incomming=set()            
    for r in reaction:
        incomming.update(set(G.predecessors(r)))
    
    return  incomming 

#####################################
#______________Upload_______________#
#####################################


#OPEN GRAPH WHICH IS IN THE TSV FILE
G=nx.read_edgelist("/Users/marwa/Downloads/MetabolicFiles200505/tripartiteID.tsv", create_using=nx.DiGraph, edgetype=None)

#CAKUT  GENES LIST
f_causal=open("/Users/marwa/Downloads/MetabolicFiles200505/CAKUT-CausalGenesFromPMC5748921.txt",'r')
f_renal=open("/Users/marwa/Downloads/MetabolicFiles200505/CAKUT-GO0072001RenalSystemDevelopment.txt",'r')
f_kidney=open("/Users/marwa/Downloads/MetabolicFiles200505/CAKUT-GO0060993KidneyMorphogenesis.txt",'r')


#list of CAKUT-CausalGenes 
CausalGene=filetolist(f_causal)

#list of CAKUT-RenalSystemDevelopment GO list
RenalSystemDevelopment=filetolist(f_renal)

#list of CAKUT-KidneyMorphogenesis GO list
KidneyMorphogenesis=filetolist(f_kidney)


#####################################
#_____________  NODE _______________#
##################################### 

nb_node =nx.DiGraph.order(G)
#print("number of nodes : ",nb_node)

#OR to get all node and not only the number of nodes
#print(list(G.nodes))



#####################################
#_____________  EDGES ______________#
##################################### 
#number of edges 

#UNDIRECTED
#nb_edges =list(G.edges)
#print("EDGES : ",nb_edges)

##DIRECTED
#print(G_sub.out_edges("WNK4"))
#print(G_sub.in_edges("WNK4"))

#####################################
#________ CausalGenes in G _________#
##################################### 
"""
TEST WHICH CAUSALGENES IS IN THE NETWORKS
"""

CausalGene=isingraph(G,CausalGene)  
  
print("Gene of causal gene in the Graph G \n",len(CausalGene),CausalGene)
print('\n')

#####################################
#______ VITAMINE D WORKFLOW ________#
#####################################   

##VITAMIN D LIST
f=open("/Users/marwa/Desktop/vitamin_D.txt",'r')
Dlist=filetolist(f)
f.close()

##FIRST NEIGHBORS  
vitd_outgoing=first_neighbors(G,Dlist)  
print(vitd_outgoing)
    

#the node related to vitamin list and which have the most connection in the graph 
print("Neighbors of this node \n" ,most_connected(vitd_outgoing))
print('\n')

##SECOND NEIGHBORS  
vitd_reaction=all_reaction(Dlist,vitd_outgoing)
print(vitd_reaction) 

vitd_incomming=second_neighbors(G,vitd_reaction) 


##MATCH WITH CAKUT FILES ?
print("wich CausalGene :",vitd_incomming.intersection(CausalGene))
print("wich KidneyMorphogenesis :",vitd_incomming.intersection(KidneyMorphogenesis))
print("wich RenalSystemDevelopment :",vitd_incomming.intersection(RenalSystemDevelopment))



#####################################
#______ VITAMINE A WORKFLOW ________#
#####################################   

##VITAMIN A LIST
g=open("/Users/marwa/Desktop/vitamin_A.txt",'r')
Alist=filetolist(g)
g.close()

##FIRST NEIGHBORS  
vita_outgoing=first_neighbors(G,Alist)  
print(vita_outgoing)
    

#the node related to vitamin list and which have the most connection in the graph 
print("Neighbors of this node \n" ,most_connected(vita_outgoing))
print('\n')

##SECOND NEIGHBORS  
vita_reaction=all_reaction(Alist,vita_outgoing)
print(vita_reaction)  

vita_incomming=second_neighbors(G,vita_reaction) 
print(vita_incomming)

##MATCH WITH CAKUT FILES ?
print("wich CausalGene :",vita_incomming.intersection(CausalGene))
print("wich KidneyMorphogenesis :",vita_incomming.intersection(KidneyMorphogenesis))
print("wich RenalSystemDevelopment :",vita_incomming.intersection(RenalSystemDevelopment))




